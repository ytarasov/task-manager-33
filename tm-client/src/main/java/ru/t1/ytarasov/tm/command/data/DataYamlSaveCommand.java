package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataYamlSaveRequest;

public final class DataYamlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-yaml-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to yaml file.";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[YAML SAVE]");
        getDomainEndpoint().saveDataYaml(new DataYamlSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
