package ru.t1.ytarasov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    private final List<String> commands = new ArrayList<>();
    private final File folder = new File("./");
    private Bootstrap bootstrap;

    public FileScanner(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.runCommand(fileName);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        init();
    }

    public void stop() {
        es.shutdown();
    }

}
