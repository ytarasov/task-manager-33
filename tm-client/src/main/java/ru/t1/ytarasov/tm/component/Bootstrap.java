package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.ytarasov.tm.api.endpoint.*;
import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.service.CommandService;
import ru.t1.ytarasov.tm.service.LoggerService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.service.TokenService;
import ru.t1.ytarasov.tm.util.SystemUtil;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_NAME = "ru.t1.ytarasov.tm.command";


    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_NAME);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void prepareStart() {
        initPid();
        initLogger();
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    public void run(@Nullable String[] args) {
        if (runArguments(args)) System.exit(0);
        prepareStart();
        System.out.println();
        runCommands();
    }

    private boolean runArguments(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            runArgument(arg);
        } catch (@Nullable final Exception e) {
            getLoggerService().error(e);
            System.out.println();
        }
        return true;
    }

    @SneakyThrows
    private void runArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void runCommands() {
        while (!Thread.currentThread().isInterrupted()) runCommand();
    }

    private void runCommand() {
        try {
            @Nullable String command = TerminalUtil.nextLine();
            runCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@Nullable final Exception e) {
            getLoggerService().error(e);
            System.out.println();
            System.out.println("[FAIL]");
        }
    }

    @SneakyThrows
    public void runCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void registry(final @NotNull AbstractCommand command) throws AbstractException {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
