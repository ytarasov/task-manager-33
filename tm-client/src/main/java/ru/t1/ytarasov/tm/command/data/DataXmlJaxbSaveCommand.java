package ru.t1.ytarasov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.request.data.DataXmlJaxbSaveRequest;

public final class DataXmlJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-xml-jaxb-save";

    @NotNull
    public static final String DESCRIPTION = "Saves to xml by JAXB";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[XML JAXB SAVE]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
