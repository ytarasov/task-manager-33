package ru.t1.ytarasov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.exception.field.RoleEmptyException;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private @NotNull final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
