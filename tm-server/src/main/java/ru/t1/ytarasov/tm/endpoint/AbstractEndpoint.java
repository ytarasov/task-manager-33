package ru.t1.ytarasov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.AbstractUserRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected Session check(@Nullable AbstractUserRequest request) throws AbstractException {
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session == null) throw new AccessDeniedException();
        return session;
    }

    protected Session check(
            @Nullable AbstractUserRequest request,
            @Nullable Role role
    ) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
