package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws AbstractException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable Status status) throws AbstractException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Status status) throws AbstractException;

    @Nullable
    List<Task> findAllTasksByProjectId(@Nullable String userId, @Nullable String projectId) throws AbstractException;

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws AbstractException;

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws AbstractException;

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, int index, @Nullable Status status) throws AbstractException;
}
