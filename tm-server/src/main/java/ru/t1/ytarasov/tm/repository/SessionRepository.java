package ru.t1.ytarasov.tm.repository;

import ru.t1.ytarasov.tm.api.repository.ISessionRepository;
import ru.t1.ytarasov.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
